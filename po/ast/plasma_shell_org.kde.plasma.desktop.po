# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Enol P. <enolp@softastur.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-30 03:36+0000\n"
"PO-Revision-Date: 2023-05-03 21:48+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"

#: contents/activitymanager/ActivityItem.qml:212
msgid "Currently being used"
msgstr ""

#: contents/activitymanager/ActivityItem.qml:252
msgid ""
"Move to\n"
"this activity"
msgstr ""

#: contents/activitymanager/ActivityItem.qml:282
msgid ""
"Show also\n"
"in this activity"
msgstr ""

#: contents/activitymanager/ActivityItem.qml:344
msgid "Configure"
msgstr ""

#: contents/activitymanager/ActivityItem.qml:363
msgid "Stop activity"
msgstr ""

#: contents/activitymanager/ActivityList.qml:143
msgid "Stopped activities:"
msgstr ""

#: contents/activitymanager/ActivityManager.qml:127
msgid "Create activity…"
msgstr ""

#: contents/activitymanager/Heading.qml:61
msgid "Activities"
msgstr ""

#: contents/activitymanager/StoppedActivityItem.qml:135
msgid "Configure activity"
msgstr ""

#: contents/activitymanager/StoppedActivityItem.qml:152
msgid "Delete"
msgstr ""

#: contents/applet/AppletError.qml:123
msgid "Sorry! There was an error loading %1."
msgstr ""

#: contents/applet/AppletError.qml:161
msgid "Copy to Clipboard"
msgstr ""

#: contents/applet/AppletError.qml:184
msgid "View Error Details…"
msgstr ""

#: contents/applet/CompactApplet.qml:71
msgid "Open %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:19
#: contents/configuration/AppletConfiguration.qml:245
msgid "About"
msgstr ""

#: contents/configuration/AboutPlugin.qml:47
msgid "Send an email to %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:61
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:125
msgid "Copyright"
msgstr ""

#: contents/configuration/AboutPlugin.qml:145 contents/explorer/Tooltip.qml:93
msgid "License:"
msgstr ""

#: contents/configuration/AboutPlugin.qml:148
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr ""

#: contents/configuration/AboutPlugin.qml:160
msgid "Authors"
msgstr ""

#: contents/configuration/AboutPlugin.qml:170
msgid "Credits"
msgstr ""

#: contents/configuration/AboutPlugin.qml:181
msgid "Translators"
msgstr ""

#: contents/configuration/AboutPlugin.qml:195
msgid "Report a Bug…"
msgstr ""

#: contents/configuration/AppletConfiguration.qml:56
msgid "Keyboard Shortcuts"
msgstr ""

#: contents/configuration/AppletConfiguration.qml:293
msgid "Apply Settings"
msgstr ""

#: contents/configuration/AppletConfiguration.qml:294
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""

#: contents/configuration/AppletConfiguration.qml:324
msgid "OK"
msgstr ""

#: contents/configuration/AppletConfiguration.qml:332
msgid "Apply"
msgstr ""

#: contents/configuration/AppletConfiguration.qml:338
#: contents/explorer/AppletAlternatives.qml:179
msgid "Cancel"
msgstr ""

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:78
msgid "Layout changes have been restricted by the system administrator"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:93
msgid "Layout:"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:107
msgid "Wallpaper type:"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:127
msgid "Get New Plugins…"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:195
msgid "Layout changes must be applied before other changes can be made"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:199
msgid "Apply Now"
msgstr ""

#: contents/configuration/ConfigurationShortcuts.qml:16
msgid "Shortcuts"
msgstr ""

#: contents/configuration/ConfigurationShortcuts.qml:28
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr ""

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr ""

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr ""

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:41
msgid " Panel Settings"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:47
#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Panel"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:71
msgid "Alignment:"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:77
msgid "Top"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:77
msgid "Left"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:78
msgid "Aligns the panel to the top if the panel is not maximized."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:78
msgid "Aligns the panel"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:87
msgid "Center"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:88
msgid "Center aligns the panel if the panel is not maximized."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid "Bottom"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid "Right"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:97
msgid "Aligns the panel to the bottom if the panel is not maximized."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:97
msgid "Aligns the panel to the right if the panel is not maximized."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:130
msgid "Visibility:"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:135
msgid "Always Visible"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:136
msgid "Makes the panel remain visible always."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:144
msgid "Auto Hide"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:145
msgid ""
"Makes the panel hidden always but reveals it when mouse enters the area "
"where the panel would have been if it were not hidden."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:164
msgid "Windows In Front"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:165
msgid ""
"Makes the panel remain visible always but maximized windows shall cover it. "
"It is revealed when mouse enters the area where the panel would have been if "
"it were not covered."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:175
msgid "Windows Behind"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:176
msgid ""
"Makes the panel remain visible always but part of the maximized windows "
"shall go below the panel as though the panel did not exist."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:205
msgid "Opacity:"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:210
msgid "Fully Opaque"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:211
msgid "Makes the panel opaque always."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:219
msgid "Adaptive"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:220
msgid "Makes the panel translucent except when some windows touch it."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:229
msgid "Translucent"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:230
msgid "Makes the panel translucent always."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:259
msgid "Floating:"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:264
msgid "Floating"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:265
msgid "Makes the panel float from the edge of the screen."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:274
msgid "Attached"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:275
msgid "Makes the panel remain attached to the edge of the screen."
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:297
msgid "Focus Shortcut"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:307
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:69
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:24
msgid "Add Widgets…"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:25
msgid "Add Spacer"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:26
msgid "More Options…"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:222
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:261
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel width:"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel height:"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:402
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr ""

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr ""

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:178
msgid "Swap with Desktop on Screen %1"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Move to Screen %1"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:193
msgid "Remove Desktop"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:267
msgid "%1 (primary)"
msgstr ""

#: contents/explorer/AppletAlternatives.qml:63
msgid "Alternative Widgets"
msgstr ""

#: contents/explorer/AppletAlternatives.qml:163
msgid "Switch"
msgstr ""

#: contents/explorer/AppletDelegate.qml:177
msgid "Undo uninstall"
msgstr ""

#: contents/explorer/AppletDelegate.qml:178
msgid "Uninstall widget"
msgstr ""

#: contents/explorer/Tooltip.qml:102
msgid "Author:"
msgstr ""

#: contents/explorer/Tooltip.qml:110
msgid "Email:"
msgstr ""

#: contents/explorer/Tooltip.qml:129
msgid "Uninstall"
msgstr ""

#: contents/explorer/WidgetExplorer.qml:129
#: contents/explorer/WidgetExplorer.qml:240
msgid "All Widgets"
msgstr ""

#: contents/explorer/WidgetExplorer.qml:194
msgid "Widgets"
msgstr "Widgets"

#: contents/explorer/WidgetExplorer.qml:202
msgid "Get New Widgets…"
msgstr ""

#: contents/explorer/WidgetExplorer.qml:251
msgid "Categories"
msgstr ""

#: contents/explorer/WidgetExplorer.qml:331
msgid "No widgets matched the search terms"
msgstr ""

#: contents/explorer/WidgetExplorer.qml:331
msgid "No widgets available"
msgstr ""
