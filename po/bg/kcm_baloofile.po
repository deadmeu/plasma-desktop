# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Svetoslav Stefanov <svetlisashkov@yahoo.com>, 2014.
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcm_baloofile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:15+0000\n"
"PO-Revision-Date: 2022-12-14 20:17+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.0\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: ui/main.qml:57
#, kde-format
msgid ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."
msgstr ""
"Това ще деактивира търсенето в Krunner и Launcher менюто и ще премахне "
"разширените метаданни от всички KDE приложения."

#: ui/main.qml:66
#, kde-format
msgid ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."
msgstr ""
"Искате ли да изтриете данните за запаметените индекси? %1 от пространството "
"ще бъдат освободени, но ако индексирането бъде повторно активирано по-късно, "
"целият индекс ще трябва да бъде отново създаден от нулата. Това може да "
"отнеме известно време, в зависимост от това колко файлове имате."

#: ui/main.qml:68
#, kde-format
msgid "Delete Index Data"
msgstr "Изтриване на индексираните данни"

#: ui/main.qml:88
#, kde-format
msgid "The system must be restarted before these changes will take effect."
msgstr "Системата трябва да бъде рестартирана, за да влязат в сила промените."

#: ui/main.qml:92
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Рестартиране"

#: ui/main.qml:99
#, kde-format
msgid ""
"File Search helps you quickly locate all your files based on their content."
msgstr ""
"Търсене на файлове ви помага бързо да намерите всичките си файлове въз "
"основа на тяхното съдържание."

#: ui/main.qml:106
#, kde-format
msgid "Enable File Search"
msgstr "Активиране на търсенето на файлове"

#: ui/main.qml:121
#, kde-format
msgid "Also index file content"
msgstr "Индексиране също и на съдържанието на файла"

#: ui/main.qml:135
#, kde-format
msgid "Index hidden files and folders"
msgstr "Индексиране на скрити файлове и папки"

#: ui/main.qml:159
#, kde-format
msgid "Status: %1, %2% complete"
msgstr "Статус: %1, %2% завършено"

#: ui/main.qml:164
#, kde-format
msgid "Pause Indexer"
msgstr "Паузиране на индексирането"

#: ui/main.qml:164
#, kde-format
msgid "Resume Indexer"
msgstr "Възобновяване на индексирането"

#: ui/main.qml:177
#, kde-format
msgid "Currently indexing: %1"
msgstr "Текущо индексиране: %1"

#: ui/main.qml:182
#, kde-format
msgid "Folder specific configuration:"
msgstr "Конфигурация, специфична за папка:"

#: ui/main.qml:209
#, kde-format
msgid "Start indexing a folder…"
msgstr "Стартиране индексиране на папка…"

#: ui/main.qml:220
#, kde-format
msgid "Stop indexing a folder…"
msgstr "Спиране индексиране на папка…"

#: ui/main.qml:271
#, kde-format
msgid "Not indexed"
msgstr "Не е индексирано"

#: ui/main.qml:272
#, kde-format
msgid "Indexed"
msgstr "Индексирано"

#: ui/main.qml:302
#, kde-format
msgid "Delete entry"
msgstr "Изтриване на записа"

#: ui/main.qml:317
#, kde-format
msgid "Select a folder to include"
msgstr "Избиране на папка за включване"

#: ui/main.qml:317
#, kde-format
msgid "Select a folder to exclude"
msgstr "Избиране на папка за изключване"
